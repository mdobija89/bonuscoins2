<?php
namespace Bold\Cron\Command;

use Magento\Framework\App\Area;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Run extends Command
{
    /**
     * @var \Magento\Cron\Model\ConfigInterface
     */
    protected $cronConfig;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var bool
     */
    private $_init;

    /**
     * Run constructor.
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Cron\Model\ConfigInterface       $cronConfig
     * @param \Magento\Framework\App\State              $state
     * @param null                                      $name
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Cron\Model\ConfigInterface $cronConfig,
        \Magento\Framework\App\State $state,
        $name = null
    ) {
        parent::__construct($name);
        $this->objectManager = $objectManager;
        $this->cronConfig = $cronConfig;
        $this->state = $state;
        $this->_init = false;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('bold:cron:run');
        $this->setDescription('Runs cron job given as param');
        $this->addArgument('job', InputArgument::REQUIRED, 'Cron job name');
        $this->setHelp(
            <<<HELP
This command will execute cron job given as param.
Run:
      <comment>%command.full_name% cron_job_name</comment>
HELP
        );
        parent::configure();
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('job');
        $job = $this->findJob($name);

        if ($job === null) {
            throw new \Exception("Job ${name} not found");
        }

        $this->invoke($job);
    }

    /**
     * @return void
     */
    protected function init()
    {
        if ($this->_init) {
            return;
        }
        $this->state->setAreaCode(Area::AREA_CRONTAB);
        $configLoader = $this->objectManager->get(\Magento\Framework\ObjectManager\ConfigLoaderInterface::class);
        $this->objectManager->configure($configLoader->load(Area::AREA_CRONTAB));

        $this->_init = true;
    }

    /**
     * @param string $name
     *
     * @return array|null
     */
    protected function findJob($name)
    {
        foreach ($this->cronConfig->getJobs() as $job) {
            if (array_key_exists($name, $job)) {
                return $job[$name];
            } else if (array_key_exists('name', $job) && $job['name'] === $name) {
                return $job;
            }
        }
        return null;
    }

    /**
     * @param array $job
     * @param array $args
     *
     * @return mixed
     */
    protected function invoke($job, $args = [])
    {
        $this->init();
        $instance = $this->objectManager->get($job['instance']);
        return call_user_func_array([$instance, $job['method']], $args);
    }
} 
<?php


namespace Cliff\BonusPoints\Api\Data;

interface DonationEntityInterface
{

    const DATE = 'date';
    const DONER_CUSTOMER_ID = 'doner_customer_id';
    const POINTS_AMOUNT = 'points_amount';
    const ENTITY_ID = 'entity_id';
    const MESSAGE = 'message';
    const TAGS = 'tags';
    const RECEIVER_CUSTOMER_ID = 'receiver_customer_id';

    /**
     * Get donationentity_id
     * @return string|null
     */
    public function getId();

    /**
     * Set donationentity_id
     * @param string $donationentityId
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setId($id);

    /**
     * Get points_amount
     * @return string|null
     */
    public function getPointsAmount();

    /**
     * Set points_amount
     * @param string $pointsAmount
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setPointsAmount($pointsAmount);

    /**
     * Get doner_customer_id
     * @return string|null
     */
    public function getDonerCustomerId();

    /**
     * Set doner_customer_id
     * @param string $donerCustomerId
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setDonerCustomerId($donerCustomerId);

    /**
     * Get receiver_customer_id
     * @return string|null
     */
    public function getReceiverCustomerId();

    /**
     * Set receiver_customer_id
     * @param string $receiverCustomerId
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setReceiverCustomerId($receiverCustomerId);

    /**
     * Get message
     * @return string|null
     */
    public function getMessage();

    /**
     * Set message
     * @param string $message
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setMessage($message);

    /**
     * Get tags
     * @return string|null
     */
    public function getTags();

    /**
     * Set tags
     * @param string $tags
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setTags($tags);

    /**
     * Get date
     * @return string|null
     */
    public function getDate();

    /**
     * Set date
     * @param string $date
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setDate($date);
}

<?php

namespace Cliff\BonusPoints\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface DonationEntitySearchResultsInterface
 * @package Cliff\BonusPoints\Api\Data
 */
interface DonationEntitySearchResultsInterface extends SearchResultsInterface
{

    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function setItems(array $items);
}

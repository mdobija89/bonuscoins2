<?php

namespace Cliff\BonusPoints\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Cliff\BonusPoints\Api\Data\DonationEntityInterface;

/**
 * Interface DonationEntityRepositoryInterface
 * @package Cliff\BonusPoints\Api
 */
interface DonationEntityRepositoryInterface
{

    /**
     * @param DonationEntityInterface $donationEntity
     * @return mixed
     */
    public function save(
        DonationEntityInterface $donationEntity
    );

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * @param DonationEntityInterface $donationEntity
     * @return mixed
     */
    public function delete(
        DonationEntityInterface $donationEntity
    );

    /**
     * @param $entityId
     * @return mixed
     */
    public function deleteById($entityId);
}

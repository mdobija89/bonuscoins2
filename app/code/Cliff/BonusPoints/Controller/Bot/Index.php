<?php

/**
 * TODO
 * 7. Miesięczna ilośc punktów w backendzie oraz cron do uzupełniania
 * https://forum.mattermost.org/t/is-there-support-for-creating-webhooks-using-mattermost-api/2400/3
 */

namespace Cliff\BonusPoints\Controller\Bot;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Cliff\BonusPoints\Processor\DonationProcessor;
use Cliff\BonusPoints\Validator\DonationValidator;
use Cliff\BonusPoints\Model\Donation;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Http
     */
    protected $request;

    /**
     * @var DonationProcessor
     */
    private $donationProcessor;

    /**
     * @var DonationValidator
     */
    private $donationValidator;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Index constructor.
     * @param Context $context
     * @param Http $request
     */
    public function __construct(
        Context $context,
        Http $request,
        DonationProcessor $donationProcessor,
        DonationValidator $donationValidator,
        ScopeConfigInterface $scopeConfig
    )
    {
        parent::__construct($context);
        $this->request = $request;
        $this->donationProcessor = $donationProcessor;
        $this->donationValidator = $donationValidator;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $this->validateRequest($this->request);
        $donation = $this->donationProcessor->prepareDonation($this->request);
        $validationResult = $this->donationValidator->validate($donation);
        if($validationResult['is_valid']){
            $this->donationProcessor->processDonation($donation);
        }
        echo $this->prepareResponse($donation, $validationResult);
    }

    /**
     * @param Donation $donation
     * @param $validationResult
     * @return string
     */
    private function prepareResponse(Donation $donation, $validationResult)
    {
        if ($validationResult['is_valid']) {
            $message = __(
                '%1 donated %2 points to %3', $donation->getDonerUsername(), $donation->getPoinsAmount(), $donation->getReceiverUsername());
        } else {
            $message = 'Sorry, the message is incorrect! ' . implode(', ', $validationResult['errors']);
        }

        return json_encode([
            'text' => $message
        ]);
    }

    /**
     * @param Http $request
     */
    private function validateRequest(Http $request)
    {
        $tokenReceived = $request->getPost('token');
        if($tokenReceived != $this->scopeConfig->getValue('bonuscoins/mattermost/token')){
            throw new \Exception('Wrong access token');
        }
    }

}
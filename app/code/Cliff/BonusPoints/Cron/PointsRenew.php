<?php
declare(strict_types=1);

namespace Cliff\BonusPoints\Cron;

use Cliff\BonusPoints\Processor\PointsRenevalProcessor;

class PointsRenew
{
    /**
     * @var PointsRenevalProcessor
     */
    private $pointsRenevalProcessor;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct( PointsRenevalProcessor $pointsRenevalProcessor)
    {
        $this->pointsRenevalProcessor = $pointsRenevalProcessor;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $this->pointsRenevalProcessor->renewAllCustomersPoints();
    }


}
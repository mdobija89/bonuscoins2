<?php

namespace Cliff\BonusPoints\Model;

class Donation
{
    /**
     * @var string
     */
    private $donerUsername;

    /**
     * @var string
     */
    private $receiverUsername;

    /**
     * @var int
     */
    private $poinsAmount;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $tags;

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function getReceiverUsername()
    {
        return $this->receiverUsername;
    }

    /**
     * @param string $receiverUsername
     */
    public function setReceiverUsername($receiverUsername)
    {
        $this->receiverUsername = $receiverUsername;
    }

    /**
     * @return int
     */
    public function getPoinsAmount()
    {
        return $this->poinsAmount;
    }

    /**
     * @param int $poinsAmount
     */
    public function setPoinsAmount($poinsAmount)
    {
        $this->poinsAmount = $poinsAmount;
    }

    /**
     * @return string
     */
    public function getDonerUsername()
    {
        return $this->donerUsername;
    }

    /**
     * @param string $donerUsername
     */
    public function setDonerUsername($donerUsername)
    {
        $this->donerUsername = $donerUsername;
    }
}
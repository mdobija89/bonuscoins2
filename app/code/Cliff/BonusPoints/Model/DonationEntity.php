<?php


namespace Cliff\BonusPoints\Model;

use Cliff\BonusPoints\Api\Data\DonationEntityInterface;

class DonationEntity extends \Magento\Framework\Model\AbstractModel implements DonationEntityInterface
{

    protected $_eventPrefix = 'donation_entity';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Cliff\BonusPoints\Model\ResourceModel\DonationEntity::class);
    }

    /**
     * Get donationentity_id
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set donationentity_id
     * @param string $donationentityId
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get points_amount
     * @return string
     */
    public function getPointsAmount()
    {
        return $this->getData(self::POINTS_AMOUNT);
    }

    /**
     * Set points_amount
     * @param string $pointsAmount
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setPointsAmount($pointsAmount)
    {
        return $this->setData(self::POINTS_AMOUNT, $pointsAmount);
    }

    /**
     * Get doner_customer_id
     * @return string
     */
    public function getDonerCustomerId()
    {
        return $this->getData(self::DONER_CUSTOMER_ID);
    }

    /**
     * Set doner_customer_id
     * @param string $donerCustomerId
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setDonerCustomerId($donerCustomerId)
    {
        return $this->setData(self::DONER_CUSTOMER_ID, $donerCustomerId);
    }

    /**
     * Get receiver_customer_id
     * @return string
     */
    public function getReceiverCustomerId()
    {
        return $this->getData(self::RECEIVER_CUSTOMER_ID);
    }

    /**
     * Set receiver_customer_id
     * @param string $receiverCustomerId
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setReceiverCustomerId($receiverCustomerId)
    {
        return $this->setData(self::RECEIVER_CUSTOMER_ID, $receiverCustomerId);
    }

    /**
     * Get message
     * @return string
     */
    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }

    /**
     * Set message
     * @param string $message
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * Get tags
     * @return string
     */
    public function getTags()
    {
        return $this->getData(self::TAGS);
    }

    /**
     * Set tags
     * @param string $tags
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setTags($tags)
    {
        return $this->setData(self::TAGS, $tags);
    }

    /**
     * Get date
     * @return string
     */
    public function getDate()
    {
        return $this->getData(self::DATE);
    }

    /**
     * Set date
     * @param string $date
     * @return \Cliff\BonusPoints\Api\Data\DonationEntityInterface
     */
    public function setDate($date)
    {
        return $this->setData(self::DATE, $date);
    }
}

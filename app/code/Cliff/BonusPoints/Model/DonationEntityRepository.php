<?php


namespace Cliff\BonusPoints\Model;

use Magento\Framework\Api\DataObjectHelper;
use Cliff\BonusPoints\Api\Data\DonationEntitySearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Cliff\BonusPoints\Model\ResourceModel\DonationEntity\CollectionFactory as DonationEntityCollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SortOrder;
use Cliff\BonusPoints\Model\ResourceModel\DonationEntity as ResourceDonationEntity;
use Cliff\BonusPoints\Api\Data\DonationEntityInterfaceFactory;
use Cliff\BonusPoints\Api\DonationEntityRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;

class DonationEntityRepository implements DonationEntityRepositoryInterface
{

    private $storeManager;

    protected $resource;

    protected $dataDonationEntityFactory;

    protected $searchResultsFactory;

    protected $donationEntityCollectionFactory;

    protected $donationEntityFactory;

    protected $dataObjectProcessor;

    protected $dataObjectHelper;


    /**
     * @param ResourceDonationEntity $resource
     * @param DonationEntityFactory $donationEntityFactory
     * @param DonationEntityInterfaceFactory $dataDonationEntityFactory
     * @param DonationEntityCollectionFactory $donationEntityCollectionFactory
     * @param DonationEntitySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceDonationEntity $resource,
        DonationEntityFactory $donationEntityFactory,
        DonationEntityInterfaceFactory $dataDonationEntityFactory,
        DonationEntityCollectionFactory $donationEntityCollectionFactory,
        DonationEntitySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->donationEntityFactory = $donationEntityFactory;
        $this->donationEntityCollectionFactory = $donationEntityCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataDonationEntityFactory = $dataDonationEntityFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Cliff\BonusPoints\Api\Data\DonationEntityInterface $donationEntity
    ) {
        /* if (empty($donationEntity->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $donationEntity->setStoreId($storeId);
        } */
        try {
            $this->resource->save($donationEntity);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the donationEntity: %1',
                $exception->getMessage()
            ));
        }
        return $donationEntity;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($donationEntityId)
    {
        $donationEntity = $this->donationEntityFactory->create();
        $this->resource->load($donationEntity, $donationEntityId);
        if (!$donationEntity->getId()) {
            throw new NoSuchEntityException(__('DonationEntity with id "%1" does not exist.', $donationEntityId));
        }
        return $donationEntity;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->donationEntityCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Cliff\BonusPoints\Api\Data\DonationEntityInterface $donationEntity
    ) {
        try {
            $this->resource->delete($donationEntity);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the DonationEntity: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($donationEntityId)
    {
        return $this->delete($this->getById($donationEntityId));
    }
}

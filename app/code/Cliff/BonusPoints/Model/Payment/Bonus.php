<?php

namespace Cliff\BonusPoints\Model\Payment;

use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Payment\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Payment\Model\InfoInterface;
use Magento\Quote\Api\Data\CartInterface;
use Cliff\BonusPoints\Processor\PaymentProcessor;
use Cliff\BonusPoints\Validator\PaymentValidator;
use Magento\Customer\Api\CustomerRepositoryInterface;

class Bonus extends AbstractMethod
{

    protected $_code = "bonus";
    protected $_canAuthorize = true;
    protected $_canCapture = true;

    /**
     * @var PaymentProcessor
     */
    private $paymentProcessor;

    /**
     * @var PaymentValidator
     */
    private $paymentValidator;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * Bonus constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param PaymentProcessor $paymentProcessor
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     * @param DirectoryHelper|null $directory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        PaymentProcessor $paymentProcessor,
        PaymentValidator $paymentValidator,
        CustomerRepositoryInterface $customerRepository,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [],
        DirectoryHelper $directory = null)
    {
        $this->paymentProcessor = $paymentProcessor;
        $this->paymentValidator = $paymentValidator;
        $this->customerRepository = $customerRepository;
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $paymentData, $scopeConfig, $logger, $resource, $resourceCollection, $data, $directory);
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this|AbstractMethod
     */
    public function capture(InfoInterface $payment, $amount)
    {
        try {
            $this->paymentProcessor->processBonusPointsPayment($payment, $amount);
            $payment->setIsTransactionClosed(1);
        } catch (\Exception $e) {
            $this->debug($payment->getData(), $e->getMessage());
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getConfigPaymentAction()
    {
        return self::ACTION_AUTHORIZE_CAPTURE;
    }

    /**
     * @return bool|AbstractMethod
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function validate()
    {
        $paymentInfo = $this->getInfoInstance();
        $quote = $paymentInfo->getQuote();
        if($quote instanceof \Magento\Quote\Model\Quote)
        {
            $amount = $paymentInfo->getQuote()->getGrandTotal();
            $customer = $paymentInfo->getQuote()->getCustomer();
        } else {
            $order = $paymentInfo->getOrder();
            $amount = $order->getGrandTotal();
            $customerId = $order->getCustomerId();
            $customer = $this->customerRepository->getById($customerId);
        }

        if(!$this->paymentValidator->isValidPaymentPointsBalance($customer, $amount)){
            throw new \Magento\Framework\Exception\LocalizedException(
                __('You have not enough coins collected.')
            );
        }

    }

    /**
     * @param CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(CartInterface $quote = null)
    {
        return parent::isAvailable($quote) && $this->paymentValidator->isValidCartPointsBalance($quote);
    }
}

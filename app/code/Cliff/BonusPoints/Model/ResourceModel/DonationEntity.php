<?php


namespace Cliff\BonusPoints\Model\ResourceModel;

class DonationEntity extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('donation_entity', 'entity_id');
    }
}

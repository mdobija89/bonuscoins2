<?php


namespace Cliff\BonusPoints\Model\ResourceModel\DonationEntity;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Cliff\BonusPoints\Model\DonationEntity::class,
            \Cliff\BonusPoints\Model\ResourceModel\DonationEntity::class
        );
    }
}

<?php

namespace Cliff\BonusPoints\Processor;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\App\Request\Http;
use Cliff\BonusPoints\Model\DonationFactory;
use Cliff\BonusPoints\Model\Donation;
use Cliff\BonusPoints\Provider\CustomerProvider;
use Cliff\BonusPoints\Model\DonationEntityRepository;
use Cliff\BonusPoints\Model\DonationEntityFactory;
use Cliff\BonusPoints\Provider\DonationProvider;

/**
 * Class DonationProcessor
 * @package Cliff\BonusPoints\Processor
 */
class DonationProcessor
{
    /**
     * @var DonationFactory
     */
    private $donationFactory;

    /**
     * @var Http
     */
    private $request;

    /**
     * @var CustomerProvider
     */
    private $customerProvider;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var DonationEntityRepository
     */
    private $donationEntityRepository;

    /**
     * @var DonationEntityFactory
     */
    private $donationEntityFactory;

    /**
     * @var DonationProvider
     */
    private $donationProvider;

    /**
     * DonationProcessor constructor.
     * @param DonationFactory $donationFactory
     * @param CustomerProvider $customerProvider
     */
    public function __construct(
        DonationFactory $donationFactory,
        CustomerProvider $customerProvider,
        CustomerRepositoryInterface $customerRepository,
        DonationEntityRepository $donationEntityRepository,
        DonationEntityFactory $donationEntityFactory,
        DonationProvider $donationProvider
    )
    {
        $this->donationFactory = $donationFactory;
        $this->customerProvider = $customerProvider;
        $this->customerRepository = $customerRepository;
        $this->donationEntityRepository = $donationEntityRepository;
        $this->donationEntityFactory = $donationEntityFactory;
        $this->donationProvider = $donationProvider;
    }

    /**
     * @param Http $request
     * @return \Cliff\BonusPoints\Model\Donation
     */
    public function prepareDonation(Http $request)
    {
        $this->donationProvider->setRequest($request);
        $donation = $this->donationFactory->create();
        $donation->setDonerUsername($this->donationProvider->prepareUsernameDoner());
        $donation->setReceiverUsername($this->donationProvider->prepareUsernameReceiver());
        $donation->setPoinsAmount($this->donationProvider->preparePointsAmount());
        $donation->setMessage($this->donationProvider->prepareMessage());
        $donation->setTags($this->donationProvider->prepareTags());
        return $donation;
    }

    /**
     * @param Donation $donation
     */
    public function processDonation(Donation $donation)
    {
        $donerCustomer = $this->customerProvider->getCustomerByChatLogin($donation->getDonerUsername());
        $receiverCustomer = $this->customerProvider->getCustomerByChatLogin($donation->getReceiverUsername());

        $this->createDonationTransaction($donerCustomer, $receiverCustomer, $donation);
    }

    /**
     * @param CustomerInterface $doner
     * @param CustomerInterface $receiver
     * @param Donation $donation
     */
    private function createDonationTransaction(CustomerInterface $doner, CustomerInterface $receiver, Donation $donation)
    {
        /** @var \Cliff\BonusPoints\Model\DonationEntity $donationEntity */
        $donationEntity = $this->donationEntityFactory->create();

        $donationEntity->setPointsAmount($donation->getPoinsAmount());
        $donationEntity->setDonerCustomerId($doner->getId());
        $donationEntity->setReceiverCustomerId($receiver->getId());
        $donationEntity->setMessage($donation->getMessage());
        $donationEntity->setTags($donation->getTags());
        $donationEntity->setDate(date('Y-m-d H:i:s'));

        $this->donationEntityRepository->save($donationEntity);
        $this->updateCustomersPoints($doner, $receiver, $donation->getPoinsAmount());
    }

    /**
     * @param CustomerInterface $doner
     * @param CustomerInterface $receiver
     * @param int $pointsAmount
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    private function updateCustomersPoints(CustomerInterface $doner, CustomerInterface $receiver, int $pointsAmount)
    {
        $donerPoints = $doner->getCustomAttribute('points_available')->getValue() - $pointsAmount;
        $doner->setCustomAttribute('points_available', $donerPoints);
        $receiverPoints = $receiver->getCustomAttribute('points_collected')->getValue() + $pointsAmount;
        $receiver->setCustomAttribute('points_collected', $receiverPoints);
        $this->customerRepository->save($doner);
        $this->customerRepository->save($receiver);
    }


}
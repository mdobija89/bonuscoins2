<?php

namespace Cliff\BonusPoints\Processor;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Payment\Model\InfoInterface;

class PaymentProcessor
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * PaymentProcessor constructor.
     * @param CustomerPointsRepository $customerPointsRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param InfoInterface $payment
     * @param $amount
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function processBonusPointsPayment(InfoInterface $payment, int $amount)
    {
        $customerId = $payment->getOrder()->getCustomerId();
        $customer = $this->customerRepository->getById($customerId);
        $points = $customer->getCustomAttribute('points_collected')->getValue() - $amount;
        $customer->setCustomAttribute('points_collected', $points);

        $this->customerRepository->save($customer);
    }

    /**
     * @param InfoInterface $payment
     * @param $amount
     */
    public function notifyOffice(InfoInterface $payment, $amount)
    {
        //TODO send email notification about prize selected by customer
    }

}
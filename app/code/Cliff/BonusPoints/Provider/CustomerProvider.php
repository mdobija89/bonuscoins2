<?php

namespace Cliff\BonusPoints\Provider;

use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;

class CustomerProvider
{
    /**
     * @var CollectionFactory
     */
    private $customerCollectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * CustomerProvider constructor.
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        CustomerRepositoryInterface $customerRepository
    )
    {
        $this->customerCollectionFactory = $collectionFactory;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param string $chatLogin
     * @return null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomerIdByChatLogin(string $chatLogin)
    {
        $customerCollection = $this->customerCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('chat_login',$chatLogin)
            ->load();

        $data = $customerCollection->getData();
        if(isset($data[0]) && isset($data[0]['entity_id'])){
            return $data[0]['entity_id'];
        }
        return null;
    }

    /**
     * @param string $chatLogin
     * @return \Magento\Customer\Api\Data\CustomerInterface|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomerByChatLogin(string $chatLogin)
    {
        $customerId = $this->getCustomerIdByChatLogin($chatLogin);
        if($customerId){
            try {
                $customer = $this->customerRepository->getById($customerId);
                return $customer;
            } catch ( \Magento\Framework\Exception\NoSuchEntityException $e) {}
        }
        return null;
    }

}
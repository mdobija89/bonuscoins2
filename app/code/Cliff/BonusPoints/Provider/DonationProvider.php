<?php

namespace Cliff\BonusPoints\Provider;

use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Cliff\BonusPoints\Exception\EmptyChatRequestException;

class DonationProvider
{
    private $request;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * DonationProvider constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct( ScopeConfigInterface $scopeConfig )
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        if(empty($this->request)){
            throw new EmptyChatRequestException('Empty chat request.');
        }
        return $this->request;
    }

    /**
     * @param mixed $request
     */
    public function setRequest(Http $request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed|\Zend\Stdlib\ParametersInterface
     */
    public function prepareUsernameDoner()
    {
        return $this->getRequest()->getPost('user_name');
    }

    /**
     * @return mixed
     */
    public function prepareUsernameReceiver()
    {
        preg_match("/@(\w)+/", $this->getRequest()->getPost('text'), $output);
        return isset($output[0]) ? str_replace('@', '', $output[0]) : null;
    }

    /**
     * @return mixed
     */
    public function preparePointsAmount()
    {
        preg_match("/\+(\d)*/", $this->getRequest()->getPost('text'), $output);
        return isset($output[0]) ? (int)($output[0]) : null;
    }

    /**
     * @return string
     */
    public function prepareMessage(): string
    {
        $trigger = $this->scopeConfig->getValue('bonuscoins/mattermost/trigger');
        return ltrim($this->getRequest()->getPost('text'), $trigger);
    }

    /**
     * @return string
     */
    public function prepareTags()
    {
        return serialize([]);
    }
}
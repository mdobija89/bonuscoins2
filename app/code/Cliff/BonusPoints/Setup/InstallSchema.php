<?php


namespace Cliff\BonusPoints\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $this->createDonationEntity($setup);
        $setup->endSetup();
    }

    private function createDonationEntity(SchemaSetupInterface $setup)
    {
        $donationEntity = $setup->getConnection()->newTable($setup->getTable('donation_entity'));

        $donationEntity->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $donationEntity->addColumn(
            'points_amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'points_amount'
        );

        $donationEntity->addColumn(
            'doner_customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'doner_customer_id'
        );

        $donationEntity->addColumn(
            'receiver_customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'receiver_customer_id'
        );

        $donationEntity->addColumn(
            'message',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'message'
        );

        $donationEntity->addColumn(
            'tags',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'tags'
        );

        $donationEntity->addColumn(
            'date',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            [],
            'date'
        );

        $setup->getConnection()->createTable($donationEntity);
    }
}

<?php

namespace Cliff\BonusPoints\Validator;

use Cliff\BonusPoints\Model\Donation;
use Cliff\BonusPoints\Provider\CustomerProvider;

class DonationValidator
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var CustomerProvider
     */
    private $customerProvider;

    /**
     * DonationValidator constructor.
     * @param CustomerProvider $customerProvider
     */
    public function __construct(
        CustomerProvider $customerProvider
    )
    {
        $this->customerProvider = $customerProvider;
    }

    /**
     * @param Donation $donation
     * @return array
     */
    public function validate(Donation $donation): array
    {
        $this->validateDoner($donation->getDonerUsername());
        $this->validateReceiver($donation->getReceiverUsername());
        $this->validateOwnDonations($donation->getDonerUsername(), $donation->getReceiverUsername());
        $this->validatePoinsAmount($donation->getPoinsAmount());
        $this->validateDonerHasEnoughPoins($donation->getDonerUsername(), $donation->getPoinsAmount());

        return [
            'is_valid' => empty($this->errors),
            'errors' =>$this->errors
        ];
    }

    /**
     * @param $donerUsername
     * @param $receiverUsername
     */
    private function validateOwnDonations($donerUsername, $receiverUsername)
    {
        if(empty($donerUsername) || !is_string($donerUsername) || empty($receiverUsername) || !is_string($receiverUsername)){
            return;
        }
        if($donerUsername == $receiverUsername) {
            $this->errors[] = 'You can not give points to yourself, cheater!';
        }
    }


    /**
     * @param $donerUsername
     */
    private function validateDoner($donerUsername)
    {
        if(empty($donerUsername) || !is_string($donerUsername) ){
            $this->errors[] = 'Not valid doner username';
            return;
        }

        $this->validateDonerExists($donerUsername);
    }

    /**
     * @param $receiverUsername
     */
    private function validateReceiver($receiverUsername)
    {
        if(empty($receiverUsername) || !is_string($receiverUsername) ){
            $this->errors[] = 'Not valid receiver username';
            return;
        }
        $this->validateReceiverExists($receiverUsername);
    }

    /**
     * @param $pointsAmount
     */
    private function validatePoinsAmount($pointsAmount)
    {
        if(empty($pointsAmount) ||!is_int($pointsAmount) ){
            $this->errors[] = 'Not valid points amount';
        }
    }

    /**
     * @param string $donerUsername
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function validateDonerExists(string $donerUsername)
    {
        $customerId = $this->customerProvider->getCustomerIdByChatLogin($donerUsername);

        if(empty($customerId)){
            $this->errors[] = 'Doner with given login not found';
        }
    }

    /**
     * @param string $receiverUsername
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function validateReceiverExists(string $receiverUsername)
    {
        $customerId = $this->customerProvider->getCustomerIdByChatLogin($receiverUsername);

        if(empty($customerId)){
            $this->errors[] = 'Receiver with given login not found';
        }
    }

    /**
     * @param $donerUsername
     * @param $pointsAmount
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function validateDonerHasEnoughPoins($donerUsername, $pointsAmount)
    {
        if(is_string($donerUsername) && is_int($pointsAmount)) {
            $doner = $this->customerProvider->getCustomerByChatLogin($donerUsername);
            if($doner) {
                if ((int)($doner->getCustomAttribute('points_available')->getValue()) < $pointsAmount) {
                    $this->errors[] = 'Not enough points to give';
                }
            }
        }
    }
}
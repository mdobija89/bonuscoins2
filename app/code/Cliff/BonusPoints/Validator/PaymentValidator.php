<?php

namespace Cliff\BonusPoints\Validator;

use Magento\Payment\Model\InfoInterface;
use Magento\Quote\Api\Data\CartInterface;
use Cliff\BonusPoints\Model\CustomerPointsRepository;
use Magento\Customer\Api\Data\CustomerInterface;

class PaymentValidator
{

    /**
     * @param InfoInterface $payment
     * @param $amount
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isValidPaymentPointsBalance(CustomerInterface $customer, $amount)
    {
        $points = $customer->getCustomAttribute('points_collected')->getValue();
        return $points >= $amount;
    }

    /**
     * @param CartInterface $quote
     * @return bool
     */
    public function isValidCartPointsBalance(CartInterface $quote)
    {
        //TODO
        return true;
    }
}
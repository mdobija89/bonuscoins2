define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'bonus',
                component: 'Cliff_BonusPoints/js/view/payment/method-renderer/bonus-method'
            }
        );
        return Component.extend({});
    }
);
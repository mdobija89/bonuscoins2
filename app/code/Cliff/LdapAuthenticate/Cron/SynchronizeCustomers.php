<?php

declare(strict_types=1);

namespace Cliff\LdapAuthenticate\Cron;

use Cliff\LdapAuthenticate\Processor\CustomerSynchronizationProcessor;

class SynchronizeCustomers
{
    /**
     * @var CustomerSynchronizationProcessor
     */
    private $customerSynchronizationProcessor;

    /**
     * @param CustomerSynchronizationProcessor $customerSynchronizationProcessor
     */
    public function __construct(CustomerSynchronizationProcessor $customerSynchronizationProcessor)
    {
        $this->customerSynchronizationProcessor = $customerSynchronizationProcessor;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->customerSynchronizationProcessor->synchronize();
    }
}
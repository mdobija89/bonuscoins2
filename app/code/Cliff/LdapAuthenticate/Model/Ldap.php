<?php

declare(strict_types=1);

namespace Cliff\LdapAuthenticate\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Ldap
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var mixed
     */
    private $connection;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        if (!$this->connection) {
            $this->createConnection();
        }
        return $this->connection;
    }

    /**
     * @return void
     */
    public function createConnection(): void
    {
        $host = $this->scopeConfig->getValue('ldap/connection/host');
        $port = $this->scopeConfig->getValue('ldap/connection/port');
        $this->connection = ldap_connect($host, (int) $port);
        ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, 3);
    }

}
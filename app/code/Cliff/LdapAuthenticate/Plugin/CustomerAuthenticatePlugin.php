<?php

declare(strict_types=1);

namespace Cliff\LdapAuthenticate\Plugin;

use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Cliff\LdapAuthenticate\Processor\AuthenticationProcessor;
use Cliff\LdapAuthenticate\Processor\CustomerCreateProcessor;
use Magento\Customer\Api\Data\CustomerInterface;

class CustomerAuthenticatePlugin
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var AuthenticationProcessor
     */
    private $authenticationProcessor;

    /**
     * @var CustomerCreateProcessor
     */
    private $customerCreateProcessor;

    /**
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerFactory $customerFactory
     * @param ManagerInterface $eventManager
     * @param AuthenticationProcessor $authenticationProcessor
     * @param CustomerCreateProcessor $customerCreateProcessor
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        CustomerFactory $customerFactory,
        ManagerInterface $eventManager,
        AuthenticationProcessor $authenticationProcessor,
        CustomerCreateProcessor $customerCreateProcessor
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
        $this->eventManager = $eventManager;
        $this->authenticationProcessor = $authenticationProcessor;
        $this->customerCreateProcessor = $customerCreateProcessor;
    }

    /**
     * @param AccountManagement $subject
     * @param callable $proceed
     * @param $username
     * @param $password
     *
     * @return CustomerInterface
     */
    public function aroundAuthenticate(AccountManagement $subject, callable $proceed, $username, $password)
    {
        $this->authenticationProcessor->authenticateLdap($username, $password);
        $customerData = $this->authenticationProcessor->getUserDataByLogin($username);
        try {
            $customer = $this->customerRepository->get($customerData['email']);
        } catch (NoSuchEntityException $e) {
            $customer = $this->customerCreateProcessor->createMagentoCustomer($customerData);
        }

        $customerModel = $this->customerFactory->create()->updateData($customer);
        $this->eventManager->dispatch(
            'customer_customer_authenticated',
            ['model' => $customerModel, 'password' => $password]
        );

        $this->eventManager->dispatch('customer_data_object_login', ['customer' => $customer]);

        return $customer;
    }

}
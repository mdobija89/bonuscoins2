<?php

declare(strict_types=1);

namespace Cliff\LdapAuthenticate\Plugin;

use Magento\Customer\Model\EmailNotification;
use Magento\Customer\Api\Data\CustomerInterface;

class DisableNewAccountEmailNotificationPlugin
{
    /**
     * @param EmailNotification $subject
     * @param callable $proceed
     * @param CustomerInterface $customer
     * @param string $type
     * @param string $backUrl
     * @param int $storeId
     * @param null $sendemailStoreId
     *
     * @return bool
     */
    public function aroundNewAccount(
        EmailNotification $subject,
        callable $proceed,
        CustomerInterface $customer,
        $type = EmailNotification::NEW_ACCOUNT_EMAIL_REGISTERED,
        $backUrl = '',
        $storeId = 0,
        $sendemailStoreId = null
    ): bool {
        return false;
    }

}
<?php

declare(strict_types=1);

namespace Cliff\LdapAuthenticate\Plugin;

use Magento\Customer\Model\Registration;

class DisableRegistrationPlugin
{
    /**
     * @param Registration $subject
     * @param $result
     *
     * @return bool
     */
    public function afterIsAllowed(Registration $subject, $result): bool
    {
        return false;
    }
}


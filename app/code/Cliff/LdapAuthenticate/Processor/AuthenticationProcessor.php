<?php

declare(strict_types=1);

namespace Cliff\LdapAuthenticate\Processor;

use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Cliff\LdapAuthenticate\Model\Ldap;

class AuthenticationProcessor
{
    /**
     * @var Ldap
     */
    private $ldap;

    /**
     * @param Ldap $ldap
     */
    public function __construct(Ldap $ldap)
    {
        $this->ldap = $ldap;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return bool
     * @throws InvalidEmailOrPasswordException
     */
    public function authenticateLdap(string $username, string $password)
    {
        $connection = $this->ldap->getConnection();

        $dn = "uid=" . $username . ",cn=users,dc=strix,dc=local";
        $bind = @ldap_bind($connection, $dn, $password);

        if (!$bind) {
            throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
        }

        return true;
    }

    /**
     * @param string $login
     *
     * @return array
     */
    public function getUserDataByLogin(string $login): array
    {
        $connection = $this->ldap->getConnection();

        $search = ldap_search($connection, "cn=users,dc=strix,dc=local", "uid=" . $login);
        $searchResult = ldap_get_entries($connection, $search);

        $data = [];
        foreach ($searchResult as $element) {
            if (is_array($element)) {
                if (empty($element['mail'][0])) {
                    continue;
                }

                if (!empty($element['gecos'][0])) {
                    $name = explode(' ', $element['gecos'][0]);
                } else {
                    $name = [$element['uid'][0], $element['uid'][0]];
                }

                $data = [
                    'email' => $element['mail'][0],
                    'uid' => $element['uid'][0],
                    'firstname' => $name[0],
                    'lastname' => $name[1]
                ];
            }
        }
        return $data;
    }

}
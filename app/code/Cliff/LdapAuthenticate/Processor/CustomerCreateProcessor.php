<?php

declare(strict_types=1);

namespace Cliff\LdapAuthenticate\Processor;

use Magento\Customer\Api\GroupManagementInterface;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Model\Metadata\FormFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Api\Data\CustomerInterface;

class CustomerCreateProcessor
{
    const POINTS_LIMIT_CONFIG = 'bonuscoins/mattermost/points_limit';

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var CustomerInterfaceFactory
     */
    private $customerFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var GroupManagementInterface
     */
    private $customerGroupManagement;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var AccountManagementInterface
     */
    private $accountManagement;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param FormFactory $formFactory
     * @param CustomerInterfaceFactory $customerFactory
     * @param StoreManagerInterface $storeManager
     * @param GroupManagementInterface $customerGroupManagement
     * @param DataObjectHelper $dataObjectHelper
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param AccountManagementInterface $accountManagement
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        FormFactory $formFactory,
        CustomerInterfaceFactory $customerFactory,
        StoreManagerInterface $storeManager,
        GroupManagementInterface $customerGroupManagement,
        DataObjectHelper $dataObjectHelper,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepositoryInterface,
        AccountManagementInterface $accountManagement,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->formFactory = $formFactory;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->customerGroupManagement = $customerGroupManagement;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->session = $customerSession;
        $this->customerRepository = $customerRepositoryInterface;
        $this->accountManagement = $accountManagement;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param array $customerData
     *
     * @return CustomerInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function createMagentoCustomer(array $customerData): CustomerInterface
    {
        $this->session->regenerateId();

        $customerDataObject = $this->prepareCustomerDataObject($customerData);
        $customerDataObject->setAddresses([]);

        $customer = $this->accountManagement->createAccount($customerDataObject, null);

        $customer->setCustomAttribute('chat_login', $customerData['uid']);
        $customer->setCustomAttribute('points_available', $this->scopeConfig->getValue(self::POINTS_LIMIT_CONFIG));
        $customer->setCustomAttribute('points_collected', 0);
        $this->customerRepository->save($customer);

        return $customer;
    }

    /**
     * @param array $customerData
     *
     * @return CustomerInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function prepareCustomerDataObject(array $customerData): CustomerInterface
    {
        $customerForm = $this->formFactory->create(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'customer_account_create',
            []
        );

        $allowedAttributes = $customerForm->getAllowedAttributes();
        $isGroupIdEmpty = isset($allowedAttributes['group_id']);

        $customerDataObject = $this->customerFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $customerDataObject,
            $customerData,
            CustomerInterface::class
        );
        $store = $this->storeManager->getStore();
        if ($isGroupIdEmpty) {
            $customerDataObject->setGroupId(
                $this->customerGroupManagement->getDefaultGroup($store->getId())->getId()
            );
        }

        $customerDataObject->setWebsiteId($store->getWebsiteId());
        $customerDataObject->setStoreId($store->getId());

        return $customerDataObject;
    }

}
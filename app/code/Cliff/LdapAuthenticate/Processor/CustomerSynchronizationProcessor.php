<?php

declare(strict_types=1);

namespace Cliff\LdapAuthenticate\Processor;

use Cliff\LdapAuthenticate\Model\Ldap;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Api\CustomerRepositoryInterface;

class CustomerSynchronizationProcessor
{
    /**
     * @var Ldap
     */
    private $ldap;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var CustomerCreateProcessor
     */
    private $customerCreateProcessor;

    /**
     * @param Ldap $ldap
     * @param ScopeConfigInterface $scopeConfig
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerCreateProcessor $customerCreateProcessor
     */
    public function __construct(
        Ldap $ldap,
        ScopeConfigInterface $scopeConfig,
        CustomerRepositoryInterface $customerRepository,
        CustomerCreateProcessor $customerCreateProcessor
    ) {
        $this->ldap = $ldap;
        $this->scopeConfig = $scopeConfig;
        $this->customerRepository = $customerRepository;
        $this->customerCreateProcessor = $customerCreateProcessor;
    }

    /**
     * @return bool
     */
    public function synchronize(): bool
    {
        $users = $this->getLdapUsers();
        foreach ($users as $user) {
            try {
                $this->customerRepository->get($user['email']);
            } catch (NoSuchEntityException $e) {
                $this->customerCreateProcessor->createMagentoCustomer($user);
            }
        }
        return true;
    }

    /**
     * @return array
     */
    private function getLdapUsers(): array
    {
        $connection = $this->ldap->getConnection();
        $username = $this->scopeConfig->getValue('ldap/connection/login');
        $password = $this->scopeConfig->getValue('ldap/connection/password');

        $filter = "mail=*@strix.net";
        $dn = "uid=" . $username . ",cn=users,dc=strix,dc=local";
        @ldap_bind($connection, $dn, $password);
        $search = ldap_search($connection, "cn=users,dc=strix,dc=local", $filter);
        $searchResult = ldap_get_entries($connection, $search);

        $users = [];
        foreach ($searchResult as $element) {
            if (is_array($element)) {
                if (empty($element['mail'][0])) {
                    continue;
                }

                if (!empty($element['gecos'][0])) {
                    $name = explode(' ', $element['gecos'][0]);
                } else {
                    $name = [$element['uid'][0], $element['uid'][0]];
                }

                $users[] = [
                    'email' => $element['mail'][0],
                    'uid' => $element['uid'][0],
                    'firstname' => $name[0],
                    'lastname' => $name[1]
                ];
            }
        }
        return $users;
    }
}
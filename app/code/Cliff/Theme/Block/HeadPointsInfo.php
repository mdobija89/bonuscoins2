<?php

namespace Cliff\Theme\Block;

use Magento\Customer\Api\CustomerRepositoryInterface;

class HeadPointsInfo extends \Magento\Framework\View\Element\Template
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * HeadPointsInfo constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
    }

    /**
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return !empty($this->customerSession->getCustomerId());
    }

    /**
     * @return int
     */
    public function getCustomerPoints(): int
    {
        $customerId = $this->customerSession->getCustomerId();
        try{
            $customer = $this->customerRepository->getById($customerId);
            $attribute = $customer->getCustomAttribute('points_collected');
            return $attribute->getValue();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e){
            return 0;
        }
    }
//TODO refaktoryzacja, niepotrzebne wyciąganie 2x tego samego customera
    /**
     * @return int|mixed
     */
    public function getPointsToGive()
    {
        $customerId = $this->customerSession->getCustomerId();
        try{
            $customer = $this->customerRepository->getById($customerId);
            $attribute = $customer->getCustomAttribute('points_available');
            return $attribute->getValue();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e){
            return 0;
        }
    }

}
<?php

namespace Cliff\Theme\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\View\Element\Template;
use Cliff\BonusPoints\Model\DonationEntityRepository;

class MainPageTransactions extends Template
{
    /**
     * @var DonationEntityRepository
     */
    private $donationEntityRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * MainPageTransactions constructor.
     * @param Context $context
     * @param DonationEntityRepository $donationEntityRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     * @param CustomerRepositoryInterface $customerRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        DonationEntityRepository $donationEntityRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        CustomerRepositoryInterface $customerRepository,
        array $data = []
    ) {
        $this->donationEntityRepository = $donationEntityRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->customerRepository = $customerRepository;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        $sortOrder = $this->sortOrderBuilder
            ->setField('date')
            ->setDirection("DESC")
            ->create();

        $searchCriteria = $this->searchCriteriaBuilder
            ->setPageSize(10)
            ->addSortOrder($sortOrder)
            ->create();
        $list = $this->donationEntityRepository->getList($searchCriteria);

        $donations = [];
        foreach ($list->getItems() as $item){
            $donations[] = [
                'doner' => $this->getCustomerName($item->getData('doner_customer_id')),
                'message' => $item->getData('message'),
                'date' => $item->getData('date')
            ];
        }
        return $donations;
    }

    /**
     * @param $customerId
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getCustomerName($customerId): string
    {
        $customer = $this->customerRepository->getById($customerId);
        return $customer->getFirstname() . ' ' . $customer->getLastname();
    }
}
